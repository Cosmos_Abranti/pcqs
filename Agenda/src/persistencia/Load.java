/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistencia;
import java.util.ArrayList;
import negocio.Agenda;

/**
 *
 * @author Josué Abranti
 */
public class Load {
    
    private ArrayList<Agenda> Agendas;
    
    public Load(){
        this.Agendas = new ArrayList();    
    }
    
    public ArrayList<negocio.Agenda> getAgendas() {
        return Agendas;
    }

    public void setAgendas(ArrayList<negocio.Agenda> Agendas) {
        this.Agendas = Agendas;
    }
}
