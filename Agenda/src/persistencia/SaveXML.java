/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import negocio.AgendaDAO;
/**
 *
 * @author Josué Abranti
 */
public class SaveXML{
    
    public static void SalvaXML(){
        XStream xstream = new XStream(new DomDriver());
        String agendas = xstream.toXML(AgendaDAO.getLoad().getAgendas());
        xstream.alias("Compromisso", negocio.Compromisso.class);
        xstream.alias("Agenda", negocio.Agenda.class);
        xstream.alias("", persistencia.Load.class);
        System.out.println(agendas);
        salvarArquivo(agendas,"Agendas.xml");   
    }

    public static void salvarArquivo(String agendas, String file) {
	File path = new File("C:\\Users\\CPQS\\Documents\\NetBeansProjects\\pcqs\\Agenda\\" + file);
        try{
            PrintWriter writer = new PrintWriter(path);
            writer.println(
            "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>"
            );           
            writer.println(agendas);
            writer.flush();
            writer.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }
    public static class LocalDateConverter extends AbstractSingleValueConverter { 

    public boolean canConvert(Class type) { 
    return (type!=null) && LocalDate.class.getPackage().equals(type.getPackage()); 
    } 

    public String toString (Object source) { 
    return source.toString(); 
    } 

    public Object fromString(String str) { 
      try { 
        return LocalDate.parse(str); 
    } catch (Exception e) { 
        return ""; 
    } 
    }
    }
}
