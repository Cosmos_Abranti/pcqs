/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistencia;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import negocio.Agenda;
import negocio.AgendaDAO;
import negocio.Compromisso;

/**
 *
 * @author Josué Abranti
 */
public class LoadXML {
    
    public static ArrayList<Agenda> carregaArquivo(){
        
        ArrayList<Agenda> Agendas = null;
        XStream xStream = new XStream(new StaxDriver());
        xStream.alias("Agendas", AgendaDAO.class);
        xStream.processAnnotations(Compromisso.class);
        try {
        Agendas = (ArrayList<Agenda>) xStream.fromXML(new FileReader("Agendas.xml"));
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return Agendas;
    }
    
    public static class LocalTimeConverter extends AbstractSingleValueConverter { 

    public boolean canConvert(Class type) { 
    return (type!=null) && LocalTime.class.getPackage().equals(type.getPackage()); 
    } 

    public String toLocalTime (Object source) { 
    return source.toString(); 
    } 

    public Object fromString(String str) { 
      try { 
        return LocalTime.parse(str); 
    } catch (Exception e) { 
        return ""; 
    } 
    }
    }
    public static class LocalDateConverter extends AbstractSingleValueConverter { 

    public boolean canConvert(Class type) { 
    return (type!=null) && LocalDate.class.getPackage().equals(type.getPackage()); 
    } 

    public String toString (Object source) { 
    return source.toString(); 
    } 

    public Object fromString(String str) { 
      try { 
        return LocalDate.parse(str); 
    } catch (Exception e) { 
        return ""; 
    } 
    }
    }
}
