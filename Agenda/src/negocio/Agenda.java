/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package negocio;

import java.util.ArrayList;
/**
 *
 * @author Josué Abranti
 */
public class Agenda {
    
    private String titulo;
    private ArrayList compromissos;
    
    AgendaDAO dao = new AgendaDAO();
    
    public Agenda(String tl){
        this.titulo = tl;
        this.compromissos = new ArrayList(); 
    }
    
    public void criaAgenda(Agenda ag){
        AgendaDAO.load.getAgendas().add(ag);
    }
    
    public void removeAgenda(Agenda ag){
        AgendaDAO.load.getAgendas().remove(ag);
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public ArrayList getCompromissos() {
        return compromissos;
    }

    public void setCompromissos(ArrayList compromissos) {
        this.compromissos = compromissos;
    }
    
      
}
