/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package negocio;

import java.time.LocalTime;
import java.time.LocalDate;

/**
 *
 * @author Josué Abranti
 */
public class Compromisso {
    
    private String titulo, assunto, local;
    private LocalDate data;
    private LocalTime horaInicio;
    private LocalTime horaFim;
    
    public Compromisso(LocalDate dt, String tl, String as,String lc, LocalTime horaInicio, LocalTime horaFim){
        this.data = dt;
        this.titulo = tl;
        this.assunto = as;
        this.local = lc;
        this.horaInicio = horaInicio;
        this.horaFim = horaFim;
    }

    @Override
    public String toString() {
        return "Compromisso{" + "titulo=" + titulo + ", assunto=" + assunto + ", local=" + local + ", data=" + data + ", horaInicio=" + horaInicio + ", horaFim=" + horaFim + '}';
        
    } 
    
    public void acresentaCompromisso(Compromisso cp, Agenda ag){
        ag.getCompromissos().add(cp);
    }
    
    public void removeCompromisso(Compromisso cp, Agenda ag){
        ag.getCompromissos().remove(cp);
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public LocalTime getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(LocalTime horaInicio) {
        this.horaInicio = horaInicio;
    }

    public LocalTime getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(LocalTime horaFim) {
        this.horaFim = horaFim;
    }   

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }
    
}
