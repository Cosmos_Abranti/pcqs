import java.util.Scanner;

public class MatrizQuadrada {
	
	Scanner Entrada = new Scanner(System.in);
	
	private int [][] matriz;
	private int [][] matrizSoma;
	private int [][] matrizAux;
	private int [][] matrizAux2;
	private int somaLinha;
	private int somaColuna;
	private int somaDiagonal;
	private static int qtd;
	private int valor;
	
	public MatrizQuadrada(int n){
		if(n > 0){
			matriz = new int [n][n];
			qtd = n;
			for(int i=0; i<n; i++){
				for(int j=0; j<n; j++){
						System.out.println("Digite o valor a ser inserido: ");
						valor = Entrada.nextInt();
						this.matriz [i][j] = valor;
				}			
			}
		}
	}
	
	public void alteraValor(int numLinha, int numColuna, int valor){
		for(int i=0; i<qtd; i++){
			for(int j=0; j<qtd; j++){
				if(i == numLinha && j == numColuna){
					matriz [i][j] = valor;
				}
			}			
		}
	}
	public int somaElementosLinha(int numLinha){
		somaLinha = 0;
		for(int j=0; j<qtd; j++){
			somaLinha += matriz [(numLinha-1)][j];
		}
		return somaLinha;
	}
	public int somaElementosColuna(int numColuna){
		somaColuna = 0;
		for(int i=0; i<qtd; i++){
			somaColuna += matriz [i][(numColuna-1)];
		}
		return somaColuna;
	}
	public int somaElementosDiagonal(){
		somaDiagonal = 0;
		for(int i=0; i<qtd; i++){
			for(int j=0; j<qtd; j++){
				if(i == j){
					somaDiagonal += matriz [i][j];
				}
			}			
		}
		return somaDiagonal;
	}
	public void somaMatrizes(int[][] matriz, int[][] matriz2){
		for(int i=0; i<qtd; i++){
			for(int j=0; j<qtd; j++){
				if(j == (qtd-1)){
					System.out.printf("\t %d \n",matriz[i][j] + matriz2[i][j]);
				}
				else if(j == 0){
					System.out.printf("%d",matriz[i][j] + matriz2[i][j]);
				}
				else{
					System.out.printf("\t %d \t",matriz[i][j] + matriz2[i][j]);
				}			
				//valor = matriz[i][j];
				//System.out.println(matriz2[i][j]);
				//matrizSoma[i][j] = matriz[i][j];
				//System.out.println(i);
				//System.out.println(j);
				//System.out.println(matriz[i][j] + matriz2[i][j]);
				//System.out.println(valor);
				//matrizSoma[i][j] = valor;
				//MatrizSoma [i][j] = matriz[i][j] + matriz2[i][j];
				//System.out.println(MatrizSoma[i][j]);
			}			
		}
		//for(int i=0; i<qtd; i++){
			//System.out.println("\n");
			//for(int j=0; j<qtd; j++){
				//System.out.println(matrizSoma[i][j]);
				//System.out.println("   ");
			//}			
		//}		
	}
	
	
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public int[][] getMatrizAux() {
		return matrizAux;
	}
	public void setMatrizAux(int[][] matrizAux) {
		this.matrizAux = matrizAux;
	}
	public int[][] getMatrizAux2() {
		return matrizAux2;
	}
	public void setMatrizAux2(int[][] matrizAux2) {
		this.matrizAux2 = matrizAux2;
	}
	public int getSomaLinha() {
		return somaLinha;
	}
	public void setSomaLinha(int somaLinha) {
		this.somaLinha = somaLinha;
	}
	public int getSomaColuna() {
		return somaColuna;
	}
	public void setSomaColuna(int somaColuna) {
		this.somaColuna = somaColuna;
	}
	public int getSomaDiagonal() {
		return somaDiagonal;
	}
	public void setSomaDiagonal(int somaDiagonal) {
		this.somaDiagonal = somaDiagonal;
	}
	public int[][] getMatrizSoma() {
		return matrizSoma;
	}
	public void setMatrizSoma(int[][] matrizSoma) {
		this.matrizSoma = matrizSoma;
	}
	public int getQtd() {
		return qtd;
	}
	public int[][] getMatriz(int i, int j) {
		return matriz;
	}
	public int[][] getMatriz() {
		return matriz;
	}
	public void setMatriz(int[][] matriz) {
		this.matriz = matriz;
	}	
}
