import java.util.Scanner; 
public class Controle {
	
	Scanner Entrada = new Scanner(System.in);
	Opcoes op = new Opcoes();
	
	private MatrizQuadrada matriz;
	private MatrizQuadrada matriz2;
	private int opcao;
	private int tam;
	private int linha;
	private int coluna;
	private int valor;
	
	public void novoControle(){
		
		opcao = op.Lista();
		
		if(opcao == 1){
			System.out.println("Digite o tamanho da matriz: ");
			tam = Entrada.nextInt();
			matriz = new MatrizQuadrada(tam);
			novoControle();
		}
		else if(opcao == 2){
			if(matriz == null){
				System.out.println("Primeiro crie uma matriz!");
				novoControle();
			}
			else{
				System.out.println("Digite o n�mero da linha que deseja alterar: ");
				linha = Entrada.nextInt();
				System.out.println("Digite o n�mero da coluna que deseja alterar: ");
				coluna = Entrada.nextInt();
				System.out.println("Digite o valor: ");
				valor = Entrada.nextInt();
				matriz.alteraValor(linha, coluna, valor);
				novoControle();
			}
		}
		else if(opcao == 3){
			if(matriz == null){
				System.out.println("Primeiro crie uma matriz!");
				novoControle();
			}
			else{
				System.out.println("Digite a linha que deseja somar: ");
				linha = Entrada.nextInt();
				System.out.println("Soma: " + matriz.somaElementosLinha(linha));
				novoControle();
			}
		}
		else if(opcao == 4){
			if(matriz == null){
				System.out.println("Primeiro crie uma matriz!");
				novoControle();
			}
			else{
				System.out.println("Digite a coluna que deseja somar: ");
				coluna = Entrada.nextInt();
				System.out.println("Soma: " + matriz.somaElementosColuna(coluna));
				novoControle();
			}
		}
		else if(opcao == 5){
			if(matriz == null){
				System.out.println("Primeiro crie uma matriz!");
				novoControle();
			}
			else{
				System.out.println("Soma: " + matriz.somaElementosDiagonal());
				novoControle();
			}
		}
		else if(opcao == 6){
			if(matriz == null){
				System.out.println("Primeiro crie uma matriz!");
				novoControle();
			}
			else{
				System.out.println("Insira valores para uma nova matriz");
				matriz2 = new MatrizQuadrada(matriz.getQtd());
				matriz.somaMatrizes(matriz.getMatriz(), matriz2.getMatriz());
				novoControle();
			}
		}
		else{
			System.out.println("Opcao Inv�lida!");
			novoControle();
		}
	}
	
}
