import java.util.Scanner; 

public class Opcoes {
	
	Scanner Entrada = new Scanner(System.in);

	public int Lista(){
	System.out.println("Selecione uma op��o: \n"
					 + "1 - Criar matriz \n"
					 + "2 - Alterar valor da matriz \n"
					 + "3 - Somar os elementos de uma determinada linha \n"
					 + "4 - Somar os elementos de uma determinada coluna \n"
					 + "5 - Somar os elementos da diagonal principal \n"
					 + "6 - Somar duas matrizes \n");
	
	return Entrada.nextInt();
	}
}