import java.util.Random;
public class Lampada {
	
	private String ligada = "n�o";
	private String queimada = "n�o";
	private int perc = 100;
	
	int potencia = 100;
	int voltagem = 120;
	
	Random gerador = new Random();
	
	public void Ligar()
	{
		if(ligada == "n�o" && queimada == "n�o"){
			perc = gerador.nextInt(99);
			
			if(perc <= 29){
				ligada = "n�o";
				queimada = "sim";
				System.out.println("L�mpada queimada!");
			}
			else{
				ligada = "sim";
				queimada = "n�o";
				System.out.println("L�mpada ligada!");
			}
		}
		else if(ligada == "n�o" && queimada == "sim"){
			System.out.println("L�mpada queimada!");
		}
		else if(ligada == "sim"){
			ligada = "n�o";	
			System.out.println("L�mpada desligada!");
		}		 
	}  
	public String getLigada() {
		return ligada;
	}
	public void setLigada(String ligada) {
		this.ligada = ligada;
	}
	public String getQueimada() {
		return queimada;
	}
	public void setQueimada(String queimada) {
		this.queimada = queimada;
	}
	public int getPotencia() {
		return potencia;
	}
	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}
	public int getVoltagem() {
		return voltagem;
	}
	public void setVoltagem(int voltagem) {
		this.voltagem = voltagem;
	}
	public int getPerc() {
		return perc;
	}
	public void setPerc(int perc) {
		this.perc = perc;
	}
	
}

