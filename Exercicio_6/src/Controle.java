import java.util.Scanner;

public class Controle {
	
	Scanner scn = new Scanner(System.in);
	Lampada lam = new Lampada();
	
	public void novoControle(){
		
		char ligar;
		
		System.out.println("Deseja alterar o estado da l�mpada(y/n) ou informa��o(i)?");

		ligar = scn.next(".").charAt(0);
		
		System.out.println(ligar);
		
		if(ligar == "y".charAt(0)){
			lam.Ligar();
			novoControle();
		}
		else if(ligar == "n".charAt(0)){
			novoControle();
		}
		else if(ligar == "i".charAt(0)){
			System.out.println("Estado alterado!");
			System.out.println("A l�mpada esta ligada: " + lam.getLigada());
			System.out.println("A l�mpada esta queimada: " + lam.getQueimada());
			System.out.println("Potencia da l�mpada: " + lam.getPotencia());
			System.out.println("Voltagem da l�mpada: " + lam.getVoltagem());
			novoControle();
		}
		else{
			System.out.println("Valor invalido!");
			novoControle();
		}
	}
}
