import java.util.ArrayList;
import java.util.Collections;

public class Cofrinho {

	private Double menorValor;
	private Moeda menorMoeda;
	private Double valorTotal;
	private int cont1c = 0;
	private int cont5c = 0;
	private int cont10c = 0;
	private int cont25c = 0;
	private int cont50c = 0;
	private int cont1r = 0;
	private ArrayList<Moeda> array;

	public Cofrinho() {
		array = new ArrayList<Moeda>();
	}

	public void adicionar(Moeda m) {
		array.add(m);
	}	
	public double calcularTotal() {
		valorTotal = 0.0;
		for (Moeda m : array) {
			valorTotal +=  m.getValor();
		}
		return valorTotal;
	}
	public double menorValor() {
		menorValor = Collections.min(array).getValor();
		//menorValor = 2.0;
		//for (Moeda m : array) {
			//if (menorValor > m.getValor()) {
				//menorValor = m.getValor();
			//}
		//}
		return menorValor;
	}
	public void menorMoeda() {
		//menorValor = 2.0;
		menorMoeda = Collections.min(array);
		//for (Moeda m : array) { 
			//if (menorValor > m.getValor()) {
				///menorMoeda =  new Moeda(m.getValor(), m.getNome());
			//}
		//}
	}	
	public void quantidades() {
		cont1c = 0;
		cont5c = 0;
		cont10c = 0;
		cont25c = 0;
		cont50c = 0;
		cont1r = 0;
		
		for (Moeda m : array) {
			
			if(m.getValor() == 0.01){
				cont1c += 1;
			}
			else if(m.getValor() == 0.05){
				cont5c += 1;
			}
			else if(m.getValor() == 0.10){
				cont10c += 1;
			}
			else if(m.getValor() == 0.25){
				cont25c += 1;
			}
			else if(m.getValor() == 0.50){
				cont50c += 1;
			}
			else if(m.getValor() == 1){
				cont1r += 1;
			}
		}
	}
	
	
	public Moeda getMenorMoeda() {
		return menorMoeda;
	}

	public void setMenorMoeda(Moeda menorMoeda) {
		this.menorMoeda = menorMoeda;
	}

	public Double getMenorValor() {
		return menorValor;
	}

	public void setMenorValor(Double menorValor) {
		this.menorValor = menorValor;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public int getCont1c() {
		return cont1c;
	}

	public void setCont1c(int cont1c) {
		this.cont1c = cont1c;
	}

	public int getCont5c() {
		return cont5c;
	}

	public void setCont5c(int cont5c) {
		this.cont5c = cont5c;
	}

	public int getCont10c() {
		return cont10c;
	}

	public void setCont10c(int cont10c) {
		this.cont10c = cont10c;
	}

	public int getCont25c() {
		return cont25c;
	}

	public void setCont25c(int cont25c) {
		this.cont25c = cont25c;
	}

	public int getCont50c() {
		return cont50c;
	}

	public void setCont50c(int cont50c) {
		this.cont50c = cont50c;
	}

	public int getCont1r() {
		return cont1r;
	}

	public void setCont1r(int cont1r) {
		this.cont1r = cont1r;
	}

	public ArrayList<Moeda> getArray() {
		return array;
	}

	public void setArray(ArrayList<Moeda> array) {
		this.array = array;
	}
	
}
