import java.util.Scanner;

public class Controle {
	
	Cofrinho cf = new Cofrinho();
	
	public void novoControle(){
		
		int opcao;
		double valor;
		String nome;

		Opcoes op = new Opcoes();
		Scanner scn = new Scanner(System.in);	
		
		opcao = op.Lista();
		
		if(opcao == 1){
			System.out.println("Digite um valor para a moeda: ");
			valor = scn.nextDouble();
			System.out.println("Digite um nome: ");
			nome = scn.next();			
			Moeda m = new Moeda(valor, nome);
			cf.adicionar(m);
			System.out.println("Moeda adicionada!");
			novoControle();
		}
		else if(opcao == 2){
			System.out.println("Total: " + cf.calcularTotal());
			novoControle();
		}
		else if(opcao == 3){
			System.out.println("Menor valor: " + cf.menorValor());
			novoControle();
		}
		else if(opcao == 4){
			cf.menorMoeda();
			System.out.println("Menor moeda: " + cf.getMenorMoeda().getNome() + "|" + cf.getMenorMoeda().getValor());
			novoControle();
		}
		else if(opcao == 5){
			cf.quantidades();
			System.out.println("Quantidade de moeda(s) de 5 centavos: " + cf.getCont1c());
			System.out.println("Quantidade de moeda(s) de 10 centavos: " + cf.getCont10c());
			System.out.println("Quantidade de moeda(s) de 25 centavos: " + cf.getCont25c());
			System.out.println("Quantidade de moeda(s) de 50 centavos: " + cf.getCont50c());
			System.out.println("Quantidade de moeda(s) de 1 real: " + cf.getCont1r());
			novoControle();
		}
		else{
			System.out.println("Op��o Invalida!");
		}
		scn.close();
	}
}