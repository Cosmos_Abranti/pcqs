public class Moeda implements Comparable<Moeda>{
	private double valor;
	private String nome;

	public Moeda(double valor, String nome) {
		this.valor = valor;
		this.nome = nome;
	}
	
	@Override
    public int compareTo(Moeda m) {
    	return Double.valueOf(this.getValor()).compareTo(m.getValor());
    }

	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}
