import java.util.Scanner;

public class Controle {
	
	int opcao;
	int posicaox;
	int posicaoy;
	int posicaox2;
	int posicaoy2;
	int raio;	 
	Circulo Circ = new Circulo();;
	Ponto ponto = new Ponto();
	Ponto ponto2 = new Ponto();;
	
	Scanner scn = new Scanner(System.in);
	Opcoes Opcoes = new Opcoes();
	
	public void novoControle(){
	
		opcao = Opcoes.Lista();
		
		if(opcao == 1){
			System.out.println("Digite um novo valor para x:");
			posicaox = scn.nextInt();
			System.out.println("Digite um novo valor para y:");
			posicaoy = scn.nextInt();
			Circ.moveCirculo(posicaox, posicaoy);
			novoControle();
		}
		else if(opcao == 2){
			Circ.aumentaRaio();
			novoControle();
		}
		else if(opcao == 3){
			Circ.diminuiRaio();
			novoControle();
		}
		else if(opcao == 4){
			System.out.println("C�rculo centrado em X: " + Circ.getPosicaox() + " e Y: "
							  + Circ.getPosicaoy() + "\n"
							  + "Raio: " + Circ.getRaio() + "\n"
							  + "Ponto nas coordendas: (" + ponto.getPosicaox()
							  + "," + ponto.getPosicaoy() + ")" );
			novoControle();
		}
		else if(opcao == 5){
			System.out.println("Digite um novo valor para x:");
			posicaox = scn.nextInt();
			System.out.println("Digite um novo valor para y:");
			posicaoy = scn.nextInt();
			Circ = new Circulo(posicaox, posicaoy);
			System.out.println("C�rculo criado!");
			novoControle();
		}
		else if(opcao == 6){
			System.out.println("Digite um novo valor para o raio:");
			raio = scn.nextInt();
			Circ = new Circulo(raio);
			System.out.println("C�rculo criado!");
			novoControle();
		}
		else if(opcao == 7){
			System.out.println("Digite um novo valor para x:");
			posicaox = scn.nextInt();
			System.out.println("Digite um novo valor para y:");
			posicaoy = scn.nextInt();
			ponto = new Ponto(posicaox, posicaoy);
			Circ.moveCirculo(ponto);
			novoControle();
		}
		else if(opcao == 8){
			System.out.println("Digite um novo valor para x:");
			ponto2.setPosicaox(scn.nextInt());
			System.out.println("Digite um novo valor para y:");
			ponto2.setPosicaoy(scn.nextInt());
			ponto.calculaPonto(ponto, ponto2);
			System.out.println("Distancia entre os pontos: " + ponto.getDist());
			novoControle();
		}
		else if(opcao == 9){
			System.out.println("Digite um novo valor para x1:");
			posicaox = scn.nextInt();
			System.out.println("Digite um novo valor para y1:");
			posicaoy = scn.nextInt();
			System.out.println("Digite um novo valor para x2:");
			posicaox2 = scn.nextInt();
			System.out.println("Digite um novo valor para y2:");
			posicaoy2 = scn.nextInt();
			ponto.calculaPonto(posicaox, posicaoy, posicaox2, posicaoy2);
			System.out.println("Distancia entre os pontos: " + ponto.getDist());
			novoControle();
		}
		else{
			System.out.println("Op��o inv�lida!");
			novoControle();
		}
	}
}
