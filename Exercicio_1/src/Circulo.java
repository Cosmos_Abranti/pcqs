public class Circulo {
	
private int posicaox = 0;
private int posicaoy = 0;
private int raio = 1;
NullPointerException excecao;

public Circulo(){
		this.posicaox = 0;
		this.posicaoy = 0;
		this.raio = 1;		
}
public Circulo(int x, int y){
	if(x == y){
		this.posicaox = x;
		this.posicaoy = y;
		this.raio = 1;		
	}
	else{
		System.out.println("Coordenadas de ponto invalidas!:");
	}
}
public Circulo(int r){
	if(r >= 0){
		this.posicaox = 0;
		this.posicaoy = 0;
		this.raio = r;		
	}
	else{
		System.out.println("Coordenadas de ponto invalidas!:");
	}
}
public Circulo(Circulo c){
	if(c.getRaio() >= 1 && c.getPosicaox() == c.getPosicaoy()){
		this.posicaox = c.getPosicaox();
		this.posicaoy = c.getPosicaoy();
		this.raio = c.getRaio();
	}
	else{
		System.out.println("Coordenadas de ponto invalidas ou raio unvalido!:");
	}
}
public Circulo(Ponto p, int r){
	if(p == null){
		excecao = new NullPointerException("Valor do ponto nulo!"); 
		throw excecao;
	}
	else if(r <= 0){
		excecao = new NullPointerException("Valor raio invalido!"); 
		throw excecao;
	}
	else{
		this.posicaox = p.getPosicaox();
		this.posicaoy = p.getPosicaoy();
		this.raio = r;
	}
}
public Circulo(int d, Ponto p){
	if(p.getPosicaox() == p.getPosicaoy()){
		if(d >= 2 && d%2 == 0){
			this.posicaox = p.getPosicaox();
			this.posicaoy = p.getPosicaoy();
			this.raio = (d/2);
		}
		else{
			System.out.println("Valor para o raio invalido!:");
		}		
	}
	else{
		System.out.println("Coordenadas de ponto invalidas!:");
	}
}

public void moveCirculo(int x, int y){
	if(x == y){
		posicaox = posicaox+x;
		posicaoy = posicaoy+y;
	}
	else{
		System.out.println("Os valores de x e y s�o diferentes!:");
	}
}

public void moveCirculo(Ponto p){
		posicaox = p.getPosicaox();
		posicaoy = p.getPosicaoy();
}

public void aumentaRaio(){
		raio = raio+1;
}

public void diminuiRaio(){
	if(raio > 1){
		raio = raio-1;
	}
	else{
		System.out.println("C�rculo n�o pode ser diminuido!:");
	}
}

public int getPosicaox() {
	return posicaox;
}

public void setPosicaox(int posicaox) {
	this.posicaox = posicaox;
}

public int getPosicaoy() {
	return posicaoy;
}

public void setPosicaoy(int posicaoy) {
	this.posicaoy = posicaoy;
}

public int getRaio() {
	return raio;
}

public void setRaio(int raio) {
	this.raio = raio;
}


}
