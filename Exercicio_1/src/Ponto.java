import java.lang.Math;

public class Ponto {
	private int posicaox = 0;
	private int posicaoy = 0;
	private double dist;
		
	public Ponto(){
		this.posicaox = 0;
		this.posicaoy = 0;
	}
	public Ponto(int x, int y){
		this.posicaox = x;
		this.posicaoy = y;
	}
	public Ponto(Ponto p){
		this.posicaox = p.getPosicaox();
		this.posicaoy = p.getPosicaoy();
	}
	
	public void calculaPonto(Ponto p1, Ponto p2){
		dist = Math.sqrt(Math.pow((p2.getPosicaox() - p1.getPosicaox()), 2) + Math.pow((p2.getPosicaoy() - p1.getPosicaoy()), 2));
	}
	public void calculaPonto(Ponto p, int x, int y){
		dist = Math.sqrt(Math.pow((x - p.getPosicaox()), 2) + Math.pow((y - p.getPosicaoy()), 2));
	}
	public void calculaPonto(int x1, int y1, int x2, int y2){
		getPosicaox();
		dist = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
	}
	
	public int getPosicaox() {
		return posicaox;
	}
	public void setPosicaox(int posicaox) {
		this.posicaox = posicaox;
	}
	public int getPosicaoy() {
		return posicaoy;
	}
	public void setPosicaoy(int posicaoy) {
		this.posicaoy = posicaoy;
	}
	public double getDist() {
		return dist;
	}
	public void setDist(double dist) {
		this.dist = dist;
	}
}
