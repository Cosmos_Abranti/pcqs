import java.util.Scanner; 

public class Opcoes {
	
	Scanner Entrada = new Scanner(System.in);

	public int Lista(){
	System.out.println("Selecione uma op��o: \n"
					 + "1 - Mover o c�rculo \n"
					 + "2 - Aumentar o tamanho de um c�rculo em uma unidade \n"
					 + "3 - Diminuir o tamanho de um c�rculo em uma unidade \n"
					 + "4 - Demonstrar o c�rculo e o ponto \n"
					 + "5 - Criar novo circulo com ponto nas coordenadas(x,y) e raio 1 \n"
					 + "6 - Criar novo circulo com ponto nas coordenadas(0,0) e raio(r) \n"
					 + "7 - Mover o circulo para o ponto nas coordenadas(x,y) \n"
					 + "8 - Calcular a distancia do ponto ao ponto das coordenadas(x,y) \n"
					 + "9 - Calcular a distancia dos pontos ao ponto das coordenadas(x,y) e (x2,y2)  \n");
	
	return Entrada.nextInt();
	}
}

