import java.util.List;
import java.util.ArrayList;

public class cadastroContas implements ContasDAO {
	
	List<ContaPoupanša> contas = new ArrayList<ContaPoupanša>();

	@Override
	public void adicionar(ContaPoupanša conta) {
		contas.add(conta);
		System.out.println("Conta adicionada!");
	}

	@Override
	public List<ContaPoupanša> buscar() {
		return contas;
	}

	@Override
	public ContaPoupanša buscar(int numeroConta) {
		ContaPoupanša contab = null;
		for(ContaPoupanša c : contas){
			if(c.getNumero() == numeroConta){
				contab = c;
			}
		}
		return contab;
	}

	@Override
	public List<ContaPoupanša> buscar(String cpfTitular) {
		List<ContaPoupanša> contac = new ArrayList<ContaPoupanša>();
		for(ContaPoupanša c : contas){
			if(c.getTitular().getCpf() == cpfTitular){
				contac = contas;
			}
		}
		return contac;
	}
	



}
