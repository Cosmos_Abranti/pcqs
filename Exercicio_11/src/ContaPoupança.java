public class ContaPoupanša {
	
	private double juros;
	private int Numero;
	private Pessoa Titular;
	
	public ContaPoupanša(int umNumero, Pessoa umTitular){
		this.Numero = umNumero;
		this.Titular = umTitular;
	}
	public void depositarRendimento(){
		
	}

	public double getJuros() {
		return juros;
	}
	public void setJuros(double valor) {
		this.juros = valor;
	}
	public int getNumero() {
		return Numero;
	}
	public void setNumero(int numero) {
		Numero = numero;
	}
	public Pessoa getTitular() {
		return Titular;
	}
	public void setTitular(Pessoa titular) {
		Titular = titular;
	}
	
	
}
