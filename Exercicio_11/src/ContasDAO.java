import java.util.List;

public interface  ContasDAO{
	
	public void adicionar(ContaPoupanša conta);
	
	public List<ContaPoupanša> buscar();
	
	public ContaPoupanša buscar(int numeroConta);
	
	public List<ContaPoupanša> buscar(String cpfTitular);
	
}
