class ContaCorrente extends ContaPoupanša {
	
	int numero;
	double saldo;
	
	public ContaCorrente(int umNumero, Pessoa umTitular) {
		super(umNumero, umTitular);
	}
	public void depositar(double valor){
		this.saldo = valor;
	}
	public double sacar(double valor){
		this.saldo -= valor;
		return valor;
	}
	public int getNumero() {
		return numero;
	}
	public double getSaldo() {
		return saldo;
	}
}