import java.util.List;

public class Pessoa {
	
	String nome;
	String cpf;
	List<ContaCorrente> conta;  
	
	public Pessoa(String umNome, String umCPF){
		this.nome = umNome;
		this.cpf = umCPF;
	}

	public String getNome() {
		return nome;
	}
	public String getCpf() {
		return cpf;
	}

	public List<ContaCorrente> getConta() {
		return conta;
	}

	public void setConta(List<ContaCorrente> conta) {
		this.conta = conta;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	
}
